using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootScript : MonoBehaviour
{
	private void OnCollisionEnter(Collision collision)
	{
	Debug.Log(gameObject.name + " collided with " + collision.gameObject.name);
		if (collision.gameObject.tag == "Player")
		{
			int score = 100 * (Random.Range(2, 4) + Random.Range(2, 4));
			GameManager.instance.AddScore(score);
			Object.Destroy(this.gameObject);
		}
	}
}

