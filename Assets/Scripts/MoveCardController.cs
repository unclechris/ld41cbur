using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCardController : MonoBehaviour {
	public string direction;

public void UseCard()
	{
		GameManager.instance.playerController.Move(direction);
		Object.Destroy(this.gameObject);		
	}
}
