using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamerFollow : MonoBehaviour {
	public Transform target;
	public float slide;
	public float height;
	public float walkDistance;

	private Transform _myTransform;

	// Use this for initialization
	void Start () {
		_myTransform = transform;
	}
	
	void LateUpdate () {
		if (target != null)
		{
			_myTransform.position = new Vector3(target.position.x+slide, target.position.y + height, target.position.z - walkDistance);
		}
	}
}
