using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

	public Canvas deathCanvas;
	public Canvas mainMenuCanvas;
	public Canvas winCanvas;

	public GameObject Player;
	public Transform PointToCheckFrom;
	public int JUMPSCOREADD = 10;
	public Animator myAnimator;
	RaycastHit hitInfo;
	public LayerMask myLayerMask;
	public int lane = 0;
	public int currentCard = 0;
	public void Move(string direction)
	{
		myAnimator.SetTrigger(direction);
		AudioManager.Instance.Play("Jump");
	}

	public void Jump()
	{
		Vector3 myPosition = Player.transform.position;
		myPosition.z += 4.0f;
		Player.transform.position = myPosition;
		CheckSafeLanding(1);
	}

	private void CheckSafeLanding(int multiplier)
	{
		Debug.Log("lane:" + lane);
		if (lane < -1 || lane > 1) { PlayerDied(); return; }
		Tile[] myTiles = GameManager.instance.instantiatedTiles.ToArray();
		currentCard = multiplier;
		int myNum;
		int.TryParse(myTiles[currentCard].tileCard.name.Substring(5, 1), out myNum);
		Debug.Log(currentCard.ToString() + ":" + myTiles[currentCard].tileCard.name.Substring(5, 1) + ":" + myNum.ToString());

		if (myNum==8)
		{
			GameManager.instance.AddScore(JUMPSCOREADD * 100);
			PlayerWon();
			return;
		}
		if (lane == -1 && (myNum != 1 && myNum != 3 && myNum != 5 && myNum != 7))
		{ PlayerDied(); return; }

		if (lane == 0 && (myNum != 2 && myNum != 3 && myNum != 6 && myNum != 7))
		{ PlayerDied(); return; }

		if (lane == 1 && (myNum != 4 && myNum != 5 && myNum != 6 && myNum != 7))
		{ PlayerDied(); return; }

		GameManager.instance.AddScore(JUMPSCOREADD * multiplier);
		GameManager.instance.TakeNextMovementCard(multiplier);		
	}

	public void PlayerDied()
	{
		AudioManager.Instance.Play("Lose");
		mainMenuCanvas.gameObject.SetActive(false);
		deathCanvas.gameObject.SetActive(true);
		Destroy(gameObject);
	}

	public void PlayerWon()
	{
		mainMenuCanvas.gameObject.SetActive(false);
		winCanvas.gameObject.SetActive(true);
		AudioManager.Instance.Play("Win");
		Destroy(gameObject);
	}




	public void DoubleJump()
	{
		Vector3 myPosition = Player.transform.position;
		myPosition.z += 8.0f;
		Player.transform.position = myPosition;
		CheckSafeLanding(2);
	}

	public void JumpLeft()
	{
		Vector3 myPosition = Player.transform.position;
		myPosition.z += 4.0f;
		myPosition.x -= 5.0f;
		Player.transform.position = myPosition;
		lane -= 1;
		CheckSafeLanding(1);
	}

	public void JumpRight()
	{
		Vector3 myPosition = Player.transform.position;
		myPosition.z += 4.0f;
		myPosition.x += 5.0f;
		Player.transform.position = myPosition;
		lane += 1;
		CheckSafeLanding(1);

	}

	public void DoubleJumpLeft()
	{
		Vector3 myPosition = Player.transform.position;
		myPosition.z += 8.0f;
		myPosition.x -= 5.0f;
		Player.transform.position = myPosition;
		lane -= 1;
		CheckSafeLanding(2);

	}

	public void DoubleJumpRight()
	{
		Vector3 myPosition = Player.transform.position;
		myPosition.z += 8.0f;
		myPosition.x += 5.0f;
		Player.transform.position = myPosition;
		lane += 1;
		CheckSafeLanding(2);
	}

	public void IdleChatter()
	{
		AudioManager.Instance.PlayIdle();
	}
}
