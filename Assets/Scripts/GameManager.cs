using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class GameManager : MonoBehaviour
{
	public static GameManager instance;
	public Transform tileParent;
	public Transform tileCardParent;
	public Transform movementCardParent;
	public PlayerMovement playerController;
	public Text txtScore;
	public Text deathScore;
	public Text winScore;
	bool lastCard = false;

	private int curScore;
	public int CurScore
	{
		get { return curScore; }
		set
		{
			if ((value > maxScore))
			{
				maxScore = value;
			}
			curScore = value;
			UpdateScoreText();
		}
	}
	int maxScore = 0;
	public Tile[] tiles;
	public Tile endTile;
	public GameObject[] movementCards;
	public Queue<Tile> instantiatedTiles = new Queue<Tile>();
	Vector3 curPos = new Vector3(0, 0, 0);

	private void Awake()
	{
		if (instance == null)
			instance = this;
		else
		{
			Destroy(gameObject);
			return;
		}
	}
	// Use this for initialization
	void Start()
	{
		InitializeTiles();
		SetScore(0);
		InitializeMoveCards();
	}

	public void InitializeTiles()
	{
		int myCard = 7;
		for (int i = 0; i < tiles.Length; i++)
		{
			if (i == 0)
			{
				myCard = 7;
			}
			else
			{
				myCard = Random.Range(5, tiles.Length);
			}
						
			Tile myTile = new Tile();
			myTile.tileInstance = SpawnTile(myCard);
			myTile.tileCard = SpawnCard(myCard);
			instantiatedTiles.Enqueue(myTile);
		}

	}
	public GameObject SpawnEndTile()
	{
		curPos.z += 4.0f;
		GameObject go = Instantiate(endTile.tileInstance, curPos, Quaternion.identity, tileParent);
		return go;

	}
	public GameObject SpawnEndCard()
	{
		GameObject go = Instantiate(endTile.tileCard, tileCardParent);
		go.transform.SetSiblingIndex(1);
		return go;
	}
	public GameObject SpawnTile(int cardIndex)
	{
		curPos.z += 4.0f;
		GameObject go = Instantiate(tiles[cardIndex].tileInstance, curPos, Quaternion.identity, tileParent);
		return go;
	}

	public GameObject SpawnCard(int cardIndex)
	{
		GameObject go = Instantiate(tiles[cardIndex].tileCard, tileCardParent);
		go.transform.SetSiblingIndex(1);
		return go;
	}

	private void SpawnMovementCard()
	{
		GameObject go = Instantiate(movementCards[Random.Range(0, movementCards.Length)], movementCardParent);
		MoveCardController myController = go.GetComponent<MoveCardController>();
		Button myButton = go.GetComponentInChildren<Button>();

		UnityEngine.Events.UnityAction action = () => { myController.UseCard(); };
		myButton.onClick.AddListener(action);
		//		action = () => { TakeNextMovementCard(); };
		//		myButton.onClick.AddListener(action);
		action = () => { Object.Destroy(go); };
		myButton.onClick.AddListener(action);
		go.transform.SetSiblingIndex(1);
	}

	private void InitializeMoveCards()
	{
		for (int i = 0; i < 5; i++)
		{
			SpawnMovementCard();
		}
	}

	public void TakeNextMovementCard(int numCards)
	{
		SpawnMovementCard();
		while (numCards > 0)
		{
			RemoveCurrentCard();
			if (!lastCard)
			{
				TakeNextTileCard();
			}
			numCards -= 1;
		}
	}
	public void RemoveCurrentCard()
	{
		Tile myTile = new Tile();
		myTile = instantiatedTiles.Dequeue();
		myTile.tileCard.SetActive(false);
		myTile.tileInstance.SetActive(false);
		GameObject.Destroy(myTile.tileCard);
		GameObject.Destroy(myTile.tileInstance);

	}
	public void TakeNextTileCard()
	{
		Tile myTile = new Tile();
		int myCard = Random.Range(1, tiles.Length);
		int lvl = Mathf.RoundToInt(curPos.z / 40.0f);
		if (lvl < 7)
		{
			if (Random.Range(0, 100) < (5*(7-lvl)))
			{
				myCard = 7;
			}
			else
			{
				while (myCard < (7 - lvl))
				{
					myCard = Random.Range(1, tiles.Length);
				}
			}
		}
		else
		myCard = Random.Range(0, tiles.Length);

		if (Random.Range(0, 100) > 95)
		{
			myTile.tileInstance = SpawnEndTile();
			myTile.tileCard = SpawnEndCard();
			instantiatedTiles.Enqueue(myTile);
			lastCard = true;
		}
		else
		{
			myTile.tileInstance = SpawnTile(myCard);
			myTile.tileCard = SpawnCard(myCard);
			instantiatedTiles.Enqueue(myTile);
		}
	}

	public void SetScore(int score)
	{
		CurScore = score;
	}

	private void UpdateScoreText()
	{
		txtScore.text = "Score: " + CurScore.ToString("d5");
		deathScore.text = "Score: " + CurScore.ToString("d5");
		winScore.text = "Score: " + CurScore.ToString("d5");
	}

	public void AddScore(int add)
	{
		CurScore += add;
	}

	public void RestartGame()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}

	public void QuitGame()
	{
		Application.Quit();
#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
#endif
	}
}
