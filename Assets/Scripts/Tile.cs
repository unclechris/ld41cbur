﻿using UnityEngine;
using UnityEngine.Audio;
using System;

[Serializable]
public class Tile
{
	public GameObject tileInstance;
	public GameObject tileCard;
	
}
